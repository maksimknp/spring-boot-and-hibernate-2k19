package edu.netcracker.hib.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "location")
@JsonIgnoreProperties({"hibernateLazyInitializer"})
public class Location {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String city;

    @Column
    private String address;

    @Column(name = "build_number")
    private int buildNumber;

    public Location(String city, String address, int buildNumber) {
        this.city = city;
        this.address = address;
        this.buildNumber = buildNumber;
    }

}
