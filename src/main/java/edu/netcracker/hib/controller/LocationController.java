package edu.netcracker.hib.controller;

import edu.netcracker.hib.entity.Location;
import edu.netcracker.hib.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/location")
public class LocationController {

    @Autowired
    private LocationService locationService;

    @GetMapping()
    public Location getLocationById(@RequestParam Long id) {
        return locationService.getLocationById(id);
    }

    @GetMapping("/all")
    public List<Location> getAllLocation() {
        return locationService.getAllLocation();
    }

    @PostMapping("/add")
    public Location createLocation(
            @RequestParam String city,
            @RequestParam String address,
            @RequestParam int buildNumber) {
        return locationService.addLocation(city, address, buildNumber);
    }

    @GetMapping("/city")
    public List<Location> getByCity(@RequestParam String city) {
        return locationService.getLocationByCity(city);
    }

}
