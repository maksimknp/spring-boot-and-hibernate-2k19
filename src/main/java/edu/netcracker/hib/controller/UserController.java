package edu.netcracker.hib.controller;

import edu.netcracker.hib.entity.User;
import edu.netcracker.hib.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/add")
    public User createLocation(
            @RequestParam String name,
            @RequestParam int age,
            @RequestParam Long locationId) {
        return userService.createUser(name, age, locationId);
    }

    //comment
    @GetMapping()
    public User getUserById(@RequestParam Long id) {
        return userService.getUserById(id);
    }

}
