package edu.netcracker.hib.service.impl;

import edu.netcracker.hib.entity.Location;
import edu.netcracker.hib.entity.User;
import edu.netcracker.hib.repository.UserRepository;
import edu.netcracker.hib.service.LocationService;
import edu.netcracker.hib.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private LocationService locationService;

    @Autowired
    private UserRepository userRepository;

    public User createUser(String name, int age, Long locationId) {
        Location location = locationService.getLocationById(locationId);
        User user = new User(name, age, location);
        userRepository.save(user);
        return user;
    }

    @Override
    public User getUserById(Long userId) {
        return userRepository.findById(userId).orElse(null);
    }

}
