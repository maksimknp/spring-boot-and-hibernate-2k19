package edu.netcracker.hib.service.impl;

import edu.netcracker.hib.entity.Location;
import edu.netcracker.hib.repository.LocationRepository;
import edu.netcracker.hib.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationServiceImpl implements LocationService {

    @Autowired
    private LocationRepository locationRepository;

    public Location addLocation(String city, String address, int buildNumber) {
        Location location = new Location(city, address, buildNumber);
        locationRepository.save(location);
        return location;
    }

    public Location getLocationById(final Long locationId) {
        return locationRepository.findById(locationId).orElse(null);
    }

    public List<Location> getAllLocation() {
        return locationRepository.findAll();
    }

    @Override
    public List<Location> getLocationByCity(String name) {
        return locationRepository.getLocationByCity(name);
    }
}
