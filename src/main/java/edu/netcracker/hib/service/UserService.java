package edu.netcracker.hib.service;

import edu.netcracker.hib.entity.User;

public interface UserService {

    User createUser(String name, int age, Long locationId);

    User getUserById(Long userId);

}
