package edu.netcracker.hib.service;

import edu.netcracker.hib.entity.Location;

import java.util.List;

public interface LocationService {

    Location addLocation(String city, String address, int buildNumber);

    Location getLocationById(Long locationId);

    List<Location> getAllLocation();

    List<Location> getLocationByCity(String name);

}
